package ru.ilyin.myapplicationlab1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MaxUnitTest {

    @Test
    public void max_1_2() {
        assertEquals(2, Lab1.max(1, 2));
    }

    @Test
    public void max_minus_1_minus_2() {
        assertEquals(-1, Lab1.max(-1, -2));
    }

    @Test
    public void max_100_100() {
        assertEquals(100, Lab1.max(100, 100));
    }

    @Test
    public void max_0_minus_2() {
        assertEquals(0, Lab1.max(0, -2));
    }


}
