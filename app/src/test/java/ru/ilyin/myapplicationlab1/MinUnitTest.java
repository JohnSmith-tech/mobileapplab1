package ru.ilyin.myapplicationlab1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class MinUnitTest {
    @Test
    public void min_1_2() {
        assertEquals(1, Lab1.min(1, 2));
    }

    @Test
    public void min_minus_10_minus_5() {
        assertEquals(-10, Lab1.min(-10, -5));
    }

    @Test
    public void min_minus_5_0() {
        assertEquals(-5, Lab1.min(-5, 0));
    }

    @Test
    public void min_10_10() {
        assertEquals(10, Lab1.min(10, 10));
    }



}