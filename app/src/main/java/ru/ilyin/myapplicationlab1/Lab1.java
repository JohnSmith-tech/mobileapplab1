package ru.ilyin.myapplicationlab1;

public class Lab1 {

    static int max(int a, int b) {
        if (a > b) return a;
        return b;
    }

    static int min(int a, int b) {
        if (a > b) return b;
        return a;
    }
}
